/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include "../W25QXXArduino/w25qxx.h"
#define FLASH_SECTOR_SIZE   512
#define FLASH_SECTOR_COUNT  2048 * 16
#define FLASH_BLOCK_SIZE 8 //??BLOCK?8???

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
  DSTATUS stat = RES_OK;

  W25QXX_Init();
  return stat;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
  DRESULT res = RES_OK;
  if(!count) return RES_PARERR; //count????0,????????
	for(;count > 0;count--){
		W25QXX_Read(buff,sector * FLASH_SECTOR_SIZE,FLASH_SECTOR_SIZE);
		sector ++;
		buff += FLASH_SECTOR_SIZE;
	}
  return res;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
  DRESULT res = RES_OK;
  if(!count) return RES_PARERR; //count????0,????????
	for(;count > 0;count --){
		W25QXX_Write((uint8_t *)buff, sector * FLASH_SECTOR_SIZE, FLASH_SECTOR_SIZE);
		sector ++;
		buff += FLASH_SECTOR_SIZE;
	}
  return res;
}



/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
  DRESULT res = RES_OK;
	switch(cmd)
	{
		case CTRL_SYNC: res = RES_OK;  break;
		case GET_SECTOR_SIZE:  *(WORD*)buff = FLASH_SECTOR_SIZE;
		         res = RES_OK;         break;
		case GET_BLOCK_SIZE:   *(WORD*)buff = FLASH_BLOCK_SIZE;
		         res = RES_OK;         break;
		case GET_SECTOR_COUNT:  *(DWORD*)buff = FLASH_SECTOR_COUNT;
		         res = RES_OK;         break;
		default:     res = RES_PARERR; break;
	}
  return res;
}

